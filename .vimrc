" OS depending setting {{{
if has("win32") || has("win16")
    let notesdir="D:/Meine-Dateien/Notes/"
    set rtp+=C:/Program\ Files\ (x86)/Vim/vimfiles/bundle/Vundle.vim
    if isdirectory('C:/tmp')
        set backupdir=C:/tmp
        set directory=C:/tmp
    endif
    if isdirectory('C:/undodir')
        set undodir=C:/undodir
    endif
else
    let notesdir="~/Meine-Dateien/Notes/"
    set rtp+=~/.vim/bundle/Vundle.vim
    if isdirectory('/Users/sebastianpech/.tmp')
        set backupdir=~/.tmp
        set directory=~/.tmp 
    endif
    if isdirectory('/Users/sebastianpech/.vim/undodir')
        set undodir=~/.vim/undodir
    endif
endif
" }}}

" Vundle {{{
set nocompatible              " required
filetype off                  " required
try
if has("win32") || has("win16")
    call vundle#begin('C:/Program\ Files\ (x86)/Vim/vimfiles/bundle')
else
    call vundle#begin()
endif
Plugin 'gmarik/Vundle.vim'
Plugin 'JuliaEditorSupport/julia-vim'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tmhedberg/SimpylFold'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'lervag/vimtex'
Plugin 'plasticboy/vim-markdown'
Plugin 'godlygeek/tabular'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-dispatch'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-repeat'
Plugin 'davidhalter/jedi-vim'
Plugin 'SirVer/ultisnips'
Plugin 'honza/vim-snippets'
Plugin 'scrooloose/syntastic'
Plugin 'altercation/vim-colors-solarized'
Plugin 'freitass/todo.txt-vim'
Plugin 'michaeljsmith/vim-indent-object'
Plugin 'xolox/vim-session'
Plugin 'xolox/vim-misc'
Plugin 'sebastianpech/term-repl'
call vundle#end()
catch
endtry
filetype plugin indent on
" }}}

" == Editor Settings ====================================================== 

" General setting {{{
set clipboard=unnamed
set history=50 " keep 50 lines of command line history
set showcmd	" display incomplete commands
set incsearch " do incremental searching
set backup
set hlsearch
set hidden " Allo buffers to be hidden
set rnu " relative line numbering
set laststatus=2 " always view statusline 
set splitbelow
set splitright	
set backspace=indent,eol,start 
set wildmenu
set scrolloff=1
set sidescrolloff=5
set omnifunc=syntaxcomplete#Complete
set foldmethod=marker
set foldtext=foldtext()
set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
set list
set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%04l,%04v]
set completeopt+=menuone
set complete=.,w,b,u,i " removed t slows comp dowm
set undofile
let g:netrw_liststyle= 3 " netrw treeview
let g:netrw_banner = 0 " netrw remove banner
set path+=** " add recurisve function to find
set cursorline " activate cursorline
set visualbell
set foldlevel=99
syntax on
set colorcolumn=80
let &showbreak = '↳ '
" }}}

" Syntax independend (indention, format) {{{
set autoindent
set textwidth=79
autocmd BufNewFile,BufRead * set formatoptions=cqlrnj 
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set fileformat=unix
" }}}

" Colorscheme {{{
try
    "let g:solarized_termcolors=256
    set background=dark
    colorscheme solarized
catch /^Vim\%((\a\+)\)\=:E185/
    colorscheme elflord
endtry
" }}}

" UTF-8 read and write {{{
if has("multi_byte")
    if &termencoding == ""
    let &termencoding = &encoding
    endif
    set encoding=utf-8
    setglobal fileencoding=utf-8
    "setglobal bomb
    set fileencodings=ucs-bom,utf-8,latin1
endif
" }}}

" Open last edited line {{{ 
if has("autocmd")
  augroup vimrcEx
    au!
    autocmd BufReadPost *
          \ if line("'\"") > 0 && line("'\"") <= line("$") |
          \   exe "normal g`\"" |
          \ endif
  augroup END
endif 
" }}}

" == Custom Mappings ====================================================== 

" Leaderkey mappings {{{
" remap leaderkey
let mapleader = ","
let maplocalleader = ","
" Spell Checking
nnoremap <leader>l0 :set nospell<cr>
nnoremap <leader>l1 :setlocal spell spelllang=de_at<cr>
nnoremap <leader>l2 :setlocal spell spelllang=en_us<cr>
" LaTeX
nnoremap <leader>lu :Runlualatex<cr>
nnoremap <leader>lp :Runlatex<cr>
" From https://www.vi-improved.org/recommendations/
nnoremap <leader>b :b <C-d>
nnoremap <leader>e :e **/
nnoremap <leader>q :b#<cr>
" Open Vim note
execute "nnoremap <leader>v :vs " . notesdir . "vim.md <cr>"
execute "nnoremap <leader>n :vs " . notesdir . "quick.md <cr> Go<esc>o# <C-R>=strftime(\"%Y-%m-%d %a %H:%M \")<cr><cr>**Subject**: "
" }}}

" Mappings {{{
" switch case of the first letter from previous word
inoremap <c-k> <esc>b~ea
inoremap <C-S> <esc>:w<cr>a
nnoremap <C-S> :w<cr>
nnoremap <space> za " Enable folding with the spacebar
nnoremap <F9> :w<CR>:Dispatch<CR>
inoremap <F9> <esc>:w<CR>:Dispatch<CR>i
" Split resizing
nnoremap <Left> :vertical resize +2<CR>
nnoremap <Right> :vertical resize -2<CR>
nnoremap <Up> :resize -2<CR>
nnoremap <Down> :resize +2<CR>
" Multiline moveing
nnoremap j gj
nnoremap k gk
" Scrolling
nnoremap <C-E> 3<C-E>
nnoremap <C-Y> 3<C-Y>
" }}}

" Custom commands {{{
" LaTeX
command! Runlualatex Dispatch! latexmk -pdf -pdflatex='lualatex --shell-escape -interaction=nonstopmode \%O \%S' -f -pv %
command! Runlatex Dispatch! latexmk -pdf -pdflatex='pdflatex --shell-escape -interaction=nonstopmode \%O \%S' -f -pv %
" Notes
execute "command! -nargs=0 Notecd cd " . notesdir 
execute "command! -nargs=0 NoteFolder vs " . notesdir 
execute "command! -complete=dir -nargs=1 NoteAdd e " . notesdir . "<args>.md"
execute "command! -nargs=+ NoteSearch vimgrep /\\c<args>/jg " . notesdir . "**/*.md | cw "
execute "command! -nargs=* NoteToPdf w | Dispatch! pandoc --latex-engine=lualatex --listings --template " . notesdir . "template.tex % -o %:r.pdf -M filename:%:t:r"
command! Wq wq
" Git
command! -nargs=* Gq Gcommit -o %:p <args>
" }}}

" == Language Specifics =================================================== 

" Lang: LaTeX {{{
augroup filetype_latex
    autocmd!
    autocmd BufRead,BufEnter,BufNewFile *.tex :call SetLaTeXOptions()
augroup END
function! SetLaTeXOptions() 
    setlocal filetype=tex
    setlocal foldmethod=expr
    let g:vimtex_fold_enable = 1
    setlocal foldexpr=vimtex#fold#level(v:lnum)
    setlocal foldtext=vimtex#fold#text()
    setlocal tabstop=2
    setlocal softtabstop=2
    setlocal shiftwidth=2
    let g:vimtex_fold_envs=0
    lcd %:p:h " switch to current folder
endfunction
" }}}

" Lang: Python {{{
" Folding
autocmd BufWinEnter *.py setlocal foldexpr=SimpylFold(v:lnum) foldmethod=expr
autocmd BufWinLeave *.py setlocal foldexpr< foldmethod< 
" }}}

" Lang: Markdown {{{
" Auto run Tabularize on | input in markdown
augroup filetype_markdown
    autocmd!
    autocmd BufNewFile,BufRead *.md :call SetMDOtions()
augroup END
function! SetMDOtions()
  setlocal formatoptions-=c
  setlocal formatoptions-=q
  setlocal formatoptions+=t
endfunction
" }}}

" Lang: Vimscript {{{
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    autocmd FileType vim :normal zM
augroup END
" }}}

" Lang: Julia {{{
autocmd FileType julia setlocal commentstring=#\ %s
" }}}

" == Plugins ============================================================== 

" Plugin: Simply fold {{{
let g:SimpylFold_docstring_preview=1
let g:SimpylFold_fold_import = 0
" }}}

" Plugin: Dispatch {{{
augroup default_dispatch
    autocmd!
    autocmd FileType python let b:dispatch = 'python %'
    autocmd FileType lua let b:dispatch = 'lua %'
    autocmd FileType julia let b:dispatch = 'julia %'
augroup END
" }}}

" Plugin: Jedi {{{ 
let g:jedi#completions_command = "<C-X><C-J>"
" }}}

" Plugin: UltiSnips {{{ 
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-n>"
let g:UltiSnipsJumpBackwardTrigger="<c-p>"
" }}} 

" Plugin: Syntasics {{{
if exists(':SyntasticCheck')
    set statusline+=%#warningmsg#
    set statusline+=%{SyntasticStatuslineFlag()}
    set statusline+=%*

    let g:syntastic_always_populate_loc_list = 1
    let g:syntastic_loc_list_height = 5
    let g:syntastic_auto_loc_list = 0
    let g:syntastic_check_on_open = 1
    let g:syntastic_check_on_wq = 1
endif
" }}}

" vim-markdown {{{ 
let g:vim_markdown_math = 1
" }}} 

" vim-multiple-cursors {{{ 
let g:multi_cursor_next_key = '<C-j>'
" }}} 

" Neovim {{{
if has('nvim')
    tnoremap <ESC> <C-\><C-n>
    " REPL Support

    " Custom Mappings
    nnoremap <F8> :call SendLine()<CR>
    vnoremap <F8> :call SendLines()<CR>

    augroup default_repls
        autocmd FileType julia nnoremap <F10> :call SendLine("include(\"".expand("%")."\")")<CR>
        autocmd FileType python let b:REPL ='ipython'
    augroup END
endif
" }}}

"  Don't save hidden and unloaded buffers in sessions.
set sessionoptions-=buffers
let g:session_autoload = 'no'


